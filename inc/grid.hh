#ifndef GRID_HH
#define GRID_HH

#include <iostream>

/*** K O N W E N C J A ***

_grid - Plansza _size x _size

 0  | 1  | 2  | . . .   
----------------
 S  |S+1 |S+2 | . . .
----------------
 2S |2S+1|2S+2| . . .

S - _size


"z zewnatrz" - funkcje move() , peek()

0,0|1,0|2,0| . . .
-------------
0,1|1,1|2,1| . . .
-------------
0,2|1,2|2,2| . . .


X = -1
O =  1
wolne = 0

*/

namespace tic
{
  class grid
  {
  public:
    grid();
    grid(int, int);
    ~grid() {delete[] _grid;}
    int  size()   const {return _size;}
    int  length() const {return _len; }
    bool move(char, int, int);
    void print();
    int  check_win();
    int  peek(int x, int y) {return _grid[_size*y+x];}
    void copy(grid&);
    void clear();
  private:
    int* _grid;       //[_size*y+x]
    int  _size = 3;   //rozmiar kwadratu
    int  _len  = 3;   //długosc linii wymagana do wygranej 
    bool move(int, int);
    int  check_line(int* tab, int size, int len);
  };

  
  
}



#endif
