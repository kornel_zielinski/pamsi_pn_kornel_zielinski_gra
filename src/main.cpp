#include <iostream>
#include "grid.hh"
#include "time.h"

void get_player_move(char player, int& x, int& y)
{
  std::string buffer;
  std::string digits = "0123456789";
  std::size_t start;
  std::size_t end;

  std::cin.clear();

  std::cout << player << ": ";
  std::getline(std::cin, buffer);

  start = buffer.find_first_of(digits);
  end = buffer.find(',');

  if(start == std::string::npos || end == std::string::npos)
    return;
  x = atoi(buffer.substr(start, end).c_str());

  buffer = buffer.substr(end);

  start = buffer.find_first_of(digits);
  end = buffer.find_last_of(digits);

  if(start == std::string::npos || end == std::string::npos)
    return;
  y = atoi(buffer.substr(start, end).c_str());
}

int min_max(tic::grid * grid, int col, int ver, int depth, bool max, char player);

void AI_move(tic::grid* grid, int& col, int& ver, char player)
{
  int depth = 6, val_count = 0, temp, val = -1000;
  int* x = new int[grid->size()*grid->size()];
  int* y = new int[grid->size()*grid->size()];
  srand(time(NULL));
  
  for(int i=0; i<grid->size(); i++)
  {
    for(int j=0; j<grid->size(); j++)
    {
      if(grid->peek(i, j) == 0)
      {
	temp = min_max(grid, i, j, depth, 1, player);

	if(temp > val)
	{
	  val = temp;
	  col = i;
	  ver = j;
	}
      }
    }
  }
  
  if(val_count == 0)
  {
    while(1)
    {
      x[0]=rand()%grid->size();
      y[0]=rand()%grid->size();
      
      if(grid->peek(x[0], y[0]) == 0)
	break;
    }
    col = x[0];
    ver = y[0];
  }
}

int min_max(tic::grid * grid, int col, int ver, int depth, bool max, char player)
{
  int win, value, value_temp, player_int;
  tic::grid temp(grid->size(), grid->length());
 
  if(depth == 0)
    return 0;

  temp.copy(*grid);
  temp.move(player, col, ver);
  
  if(player == 'X')
    player_int = -1;
  else
    player_int = 1;
  
  win=temp.check_win();
  
  if(win == player)
    return 1*depth;
  else if(win == 2)
    return 0;
  else if(win == -player)
    return -1*depth;
  
  if(player == 'X')
    player = 'O';
  else
    player = 'X';
  
  if(max)
  {
    value=-1000;
    for(int i=0; i<temp.size(); i++)
    {
      for(int j=0; j<temp.size(); j++)
      {
	if(grid->peek(i, j) == 0)
	  value_temp = min_max(&temp, i, j, depth-1, !max, player);
	if(value < value_temp)
	  value = value_temp;
      }
    }
  }

  else
  {
    value=1000;
    for(int i=0; i<temp.size(); i++)
    {
      for(int j=0; j<temp.size(); j++)
      {
	if(grid->peek(i, j) == 0)
	  value_temp = min_max(&temp, i, j, depth-1, !max, player);
	if(value > value_temp)
	  value = value_temp;
      }
    }
  }
  return value;
}

int main()
{
  tic::grid *grid;
  char player = 'X', temp;
  int x,y, win, size=3, length=3;
  bool AI=true;
  
  while(1)
  {
    std::cout << "Podaj rozmiar planszy: ";
    std::cin >> size;
    std::cin.clear();
    std::cin.ignore(256, '\n');
    
    std::cout << "Podaj dlugosc linii: ";
    std::cin >> length;
    std::cin.clear();
    std::cin.ignore(256, '\n');

    std::cout << "AI? : ";
    temp = std::cin.get();
    std::cin.clear();
    std::cin.ignore(256, '\n');
    
    if(temp == 'n')
      AI = false;

    player = 'X';
  
    if(size<3)
      size=3;
    if(length<3)
      length=3;
    if(length > size)
      length = size;

    grid = new tic::grid(size, length);
    
    while((win = grid->check_win())==0)
    {	
      std::cout << '\n';
      grid->print();

      if(AI && player == 'O')
	AI_move(grid, x, y, player);
      else
	get_player_move(player, x, y);
      if(grid->move(player, x, y))
      {
	if(player == 'X')
	  player = 'O';
	else
	  player = 'X';
      }

    }

    grid->print();
    
    switch(win)
    {
    case 2:
      std::cout << "Remis.\n";
      break;
    case -1:
      std::cout << "X wygral.\n";
      break;
    case 1:
      std::cout << "O wygralo.\n";
      break;
    }
    grid->clear();
    delete grid;
    
    std::cout << "Jeszcze Raz?\n";
    temp = std::cin.get();
    std::cin.clear();
    std::cin.ignore(256, '\n');
    if(temp!='t')
      break;
  }
  return 0;
}
